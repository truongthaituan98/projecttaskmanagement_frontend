import axios from "axios";
import { GET_ERRORS, GET_PROJECTS, GET_PROJECT, DELETE_PROJECT } from "./types";
import { bindActionCreators } from "redux";

export const createProject = (project, history) => async dispatch => {
  try {
    const res = await axios.post(`http://localhost:8080/api/projects`, project);
    history.push("/dashboard");
    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
  } catch (err) {
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data
    });
  }
};
export const getProjects = () => async dispatch => {
  const res = await axios.get(`http://localhost:8080/api/projects`);
  dispatch({
    type: GET_PROJECTS,
    payload: res.data
  });
};

export const getProject = (projectIdentifier) => async dispatch => {
  const res = await axios.get(`http://localhost:8080/api/projects/${projectIdentifier}`);
  dispatch({
    type: GET_PROJECT,
    payload: res.data
  });
};

export const updateProject = (projectIdentifier, project, history) => async dispatch => {
  try {
    const res = await axios.put(`http://localhost:8080/api/projects/${projectIdentifier}`, project);
    history.push("/dashboard");
    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
  } catch (err) {
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data
    });
  }
};

export const deleteProject = (projectIdentifier) => async dispatch => {
  if (
    window.confirm(
      "Are you sure? This will delete the project and all the data related to it"
    )
  ) {
  const res = await axios.delete(`http://localhost:8080/api/projects/${projectIdentifier}`);
  dispatch({
    type: DELETE_PROJECT,
    payload: res.data
  });
}
};