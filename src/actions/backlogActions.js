import axios from "axios";
import { GET_ERRORS, GET_BACKLOG, GET_PROJECT_TASK, DELETE_PROJECT_TASK } from "./types";

export const addProjectTask = (
    projectIdentifier,
  projectTask,
  history
) => async dispatch => {
    try {
  await axios.post(`http://localhost:8080/api/backlog/${projectIdentifier}`, projectTask);
  history.push(`/projectBoard/${projectIdentifier}`);
  dispatch({
    type: GET_ERRORS,
    payload: {}
  });
    }catch(err){
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data
          });
    }
};

export const getBacklog = projectIdentifier => async dispatch => {
    try {
      const res = await axios.get(`http://localhost:8080/api/backlog/${projectIdentifier}`);
      dispatch({
        type: GET_BACKLOG,
        payload: res.data
      });
    } catch (err) {
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data
          });
    }
  };
  export const getProjectTask = (projectIdentifier, pt_id, history) => async dispatch => {
    try {
        const res = await axios.get(`http://localhost:8080/api/backlog/${projectIdentifier}/${pt_id}`);
  dispatch({
    type: GET_PROJECT_TASK,
    payload: res.data
  });
    }catch(err){
      history.push("/dashboard")
    }
};

export const updateTask = (projectIdentifier, pt_id,newTask, history) => async dispatch => {
    try {
      const res = await axios.patch(`http://localhost:8080/api/backlog/${projectIdentifier}/${pt_id}`, newTask);
      history.push(`/projectBoard/${projectIdentifier}`);
      dispatch({
        type: GET_ERRORS,
        payload: {}
      });
    } catch (err) {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    }
  };
  
export const deleteTask = (projectIdentifier, pt_id) => async dispatch => {
    if (
      window.confirm(
        "Are you sure? This will delete the task and all the data related to it"
      )
    ) {
    const res = await axios.delete(`http://localhost:8080/api/backlog/${projectIdentifier}/${pt_id}`);
    dispatch({
      type: DELETE_PROJECT_TASK,
      payload: pt_id
    });
  }
  };