import React, { Component } from "react";
import { connect } from "react-redux";
import { getProjectTask, updateTask } from "../../../actions/backlogActions";
import PropTypes from "prop-types";

class UpdateProjectTask extends Component {
    constructor() {
        super();
    
        this.state = {
            taskId: "",
            projectTaskSequence: "",
            summary: "",
            acceptanceCriteria: "",
            status: "",
            priority: "",
            dueDate: "",
            createdAt: "",
            updatedAt: "",
            errors: {}
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
      }
    
      componentDidMount() {
        const { projectIdentifier, pt_id } = this.props.match.params;
        this.props.getProjectTask(projectIdentifier, pt_id, this.props.history);
      }
    
      componentWillReceiveProps(nextProps) {
        const {
            taskId,
          projectTaskSequence,
          summary,
          acceptanceCriteria,
          status,
          priority,
          dueDate,
          createdAt,
          updatedAt
        } = nextProps.project_task;
    
        this.setState({
            taskId,
          projectTaskSequence,
          summary,
          acceptanceCriteria,
          status,
          priority,
          dueDate,
          createdAt,
          updatedAt
        });
      }
    
      onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
      }
    
      onSubmit(e) {
        e.preventDefault();
    
        const UpdateProjectTask = {
          summary: this.state.summary,
          acceptanceCriteria: this.state.acceptanceCriteria,
          status: this.state.status,
          priority: this.state.priority,
          dueDate: this.state.dueDate,
          createdAt: this.state.createdAt,
          updatedAt: this.state.updatedAt
        };
        const { projectIdentifier, pt_id } = this.props.match.params;
        console.log(UpdateProjectTask);
        this.props.updateTask(projectIdentifier, pt_id,UpdateProjectTask, this.props.history);
      }
    

  render() {
    const { errors } = this.state;
    const { projectIdentifier } = this.props.match.params;
    return (
      <div className="add-PBI">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <a href="#" className="btn btn-light">
                Back to Project Board
              </a>
              <h4 className="display-4 text-center">Update Project Task</h4>
              <p className="lead text-center">
                Project Name: {projectIdentifier} | Project Task ID:{" "}
                {this.state.projectTaskSequence}{" "}
              </p>
              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control form-control-lg"
                    name="summary"
                    placeholder="Project Task summary"
                    value={this.state.summary}
                    onChange={this.onChange}
                  />
                    <p style={{float:'left', color:'red'}}>{errors.summary}</p>
                </div>
                <div className="form-group">
                  <textarea
                    className="form-control form-control-lg"
                    placeholder="Acceptance Criteria"
                    name="acceptanceCriteria"
                    value={this.state.acceptanceCriteria}
                    onChange={this.onChange}
                  />
                    <p style={{float:'left', color:'red'}}>{errors.acceptanceCriteria}</p>
                </div>
                <h6>Due Date</h6>
                <div className="form-group">
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    name="dueDate"
                    value={this.state.dueDate}
                    onChange={this.onChange}
                  />
                   <p style={{float:'left', color:'red'}}>{errors.dueDate}</p>
                </div>
                <div className="form-group">
                  <select
                    className="form-control form-control-lg"
                    name="priority"
                    value={this.state.priority}
                    onChange={this.onChange}
                  >
                    <option value={0}>Select Priority</option>
                    <option value={1}>High</option>
                    <option value={2}>Medium</option>
                    <option value={3}>Low</option>
                  </select>
                  
                </div>

                <div className="form-group">
                  <select
                    className="form-control form-control-lg"
                    name="status"
                    value={this.state.status}
                    onChange={this.onChange}
                  >
                    <option value="">Select Status</option>
                    <option value="TO_DO">TO DO</option>
                    <option value="IN_PROGRESS">IN PROGRESS</option>
                    <option value="DONE">DONE</option>
                  </select>
                  <p style={{float:'left', color:'red'}}>{errors.status}</p>
                </div>
                <h6>Created At</h6>
                <div className="form-group">
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    name="createdAt"
                    value={this.state.createdAt}
                    onChange={this.onChange}
                  />
                </div>
                <h6>Updated At</h6>
                <div className="form-group">
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    name="updatedAt"
                    value={this.state.updatedAt}
                    onChange={this.onChange}
                  />
                </div>
                <input
                  type="submit"
                  className="btn btn-primary btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

UpdateProjectTask.propTypes = {
    getProjectTask: PropTypes.func.isRequired,
    task: PropTypes.object.isRequired,
    updateTask: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired
  };
  
  const mapStateToProps = state => ({
    project_task: state.backlog.task,
    errors: state.errors
  });
  
  export default connect(
    mapStateToProps,
    {updateTask, getProjectTask }
  )(UpdateProjectTask);