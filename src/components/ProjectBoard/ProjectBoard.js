import React, { Component } from "react";
import { Link } from "react-router-dom";
import Backlog from "./Backlog";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getBacklog } from "../../actions/backlogActions";
class ProjectBoard extends Component {
      //constructor to handle errors
  constructor() {
    super();
    this.state = {
      errors: {}
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }
  componentDidMount() {
    const { projectIdentifier } = this.props.match.params;
    this.props.getBacklog(projectIdentifier);
  }
  render() {
    const { projectIdentifier } = this.props.match.params;
    const { tasks } = this.props.backlog;
    const { errors } = this.state;
    let BoardContent;

    const boardAlgorithm = (errors, tasks) => {
      if (tasks.length < 1) {
        if (errors.projectNotFound) {
            console.log(errors.projectNotFound)
          return (
            <div className="alert alert-danger text-center" role="alert">
              {errors.projectNotFound}
            </div>
          );
        
        } else {
          return (
            <div className="alert alert-info text-center" role="alert">
              No Project Tasks on this board
            </div>
          );
        }
      } else {
        return <Backlog project_tasks_prop={tasks} projectIdentifier={projectIdentifier}/>;
      }
    };

    BoardContent = boardAlgorithm(errors, tasks);
    // console.log(tasks)
    return (
      <div className="container">
        <Link to={`/addProjectTask/${projectIdentifier}`} className="btn btn-primary mb-3">
          <i className="fas fa-plus-circle"> Create Project Task</i>
        </Link>
        <br />
        <hr />
        {BoardContent}
      </div>
    );
  }
}


ProjectBoard.propTypes = {
    backlog: PropTypes.object.isRequired,
    getBacklog: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired
  };
  
  const mapStateToProps = state => ({
    backlog: state.backlog,
    errors: state.errors
  });
  
  export default connect(
    mapStateToProps,
    { getBacklog }
  )(ProjectBoard);