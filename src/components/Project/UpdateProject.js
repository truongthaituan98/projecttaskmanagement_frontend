
import React, { Component } from "react";
import { getProject, updateProject } from "../../actions/projectActions";
import PropTypes from "prop-types";
import { connect } from "react-redux";


class UpdateProject extends Component {
  //set state
  constructor() {
    super();

    this.state = {
      projectIdentifier: "",
      projectName: "",
      description: "",
      startDate: "",
      endDate: "",
      createdAt: "",
      updatedAt: "",
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const {
        projectIdentifier,
        projectName,
        description,
        startDate,
        endDate,
        createdAt,
        updatedAt
    } = nextProps.project;

    this.setState({
        projectIdentifier,
        projectName,
        description,
        startDate,
        endDate,
        createdAt,
        updatedAt
    });
    if (nextProps.errors) {
        this.setState({ errors: nextProps.errors });
      }
  }

  componentDidMount() {
    const { projectIdentifier } = this.props.match.params;
    this.props.getProject(projectIdentifier);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();

    const updateProject = {
      projectIdentifier: this.state.projectIdentifier,
      projectName: this.state.projectName,
      description: this.state.description,
      startDate: this.state.startDate,
      endDate: this.state.endDate,
      createdAt: this.state.createdAt,
      updatedAt: this.state.updatedAt
    };
    // console.log(updateProject)
    const { projectIdentifier } = this.props.match.params;
    // console.log(projectIdentifier)
    this.props.updateProject(updateProject.projectIdentifier, updateProject, this.props.history);
  }

  render() {
    const { errors } = this.state;
    return (
      <div className="project">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h5 className="display-4 text-center">Update Project form</h5>
              <hr />
              <form onSubmit={this.onSubmit}>
              <div className="form-group">
                  <input
                    type="text"
                    className="form-control form-control-lg"
                    placeholder="Unique Project ID"
                    name="projectIdentifier"
                    value={this.state.projectIdentifier}
                    onChange={this.onChange}
                    disabled
                  />
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control form-control-lg "
                    placeholder="Project Name"
                    name="projectName"
                    value={this.state.projectName}
                    onChange={this.onChange}/>
                   <p style={{float:'left', color:'red'}}>{errors.projectName}</p>
                </div>
                <div className="form-group">
                  <textarea
                    className="form-control form-control-lg"
                    placeholder="Project Description"
                    name="description"
                    onChange={this.onChange}
                    value={this.state.description}/>
                   <p style={{float:'left', color:'red'}}>{errors.description}</p>
                </div>
                <h6 style={{float:'left'}}>Start Date</h6>
                <div className="form-group">
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    name="startDate"
                    value={this.state.startDate}
                    onChange={this.onChange}
                  />
                  <p style={{float:'left', color:'red'}}>{errors.startDate}</p>
                </div>
                <h6 style={{float:'left'}}>End Date</h6>
                <div className="form-group">
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    name="endDate"
                    value={this.state.endDate}
                    onChange={this.onChange}
                  />
                   <p style={{float:'left', color:'red'}}>{errors.endDate}</p>
                </div>
                <h6 style={{float:'left'}}>Created At</h6>
                <div className="form-group">
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    name="createdAt"
                    value={this.state.createdAt}
                    onChange={this.onChange}
                  />
                </div>
                <h6 style={{float:'left'}}>Updated At</h6>
                <div className="form-group">
                  <input
                    type="date"
                    className="form-control form-control-lg"
                    name="updatedAt"
                    value={this.state.updatedAt}
                    onChange={this.onChange}
                  />
                </div>
                <input
                  type="submit" value = "Submit"
                  className="btn btn-primary btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

UpdateProject.propTypes = {
  getProject: PropTypes.func.isRequired,
  updateProject: PropTypes.func.isRequired,
  project: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  project: state.project.project,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { getProject, updateProject }
)(UpdateProject);